
#include <stdio.h>
#include "mpi.h"
#include <math.h>
#include <stdlib.h>

int main(int argc, char** argv) {

    int rank;
    int p;
    int size = 10;
    int counter = 0;
    // run command for 8 times -> mpiexec -np 8 Cs390_Project1.exe
    MPI_Init(&argc, &argv);

    // rank and size
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &p);
    int M = p - 1;

    int* vector_x = malloc(M * sizeof(int)); // &vector_x[0]

    MPI_Status status;
    int *A;
    // dinamik olarak M size inda array of array yaratiyor
    A = (int*)malloc(sizeof(int) * M * M);


    if (rank == 0) {  // Process 0 random bir matrix olu�turuyor. Matrix n x m ve buradaki n process say�s� kadar.

        printf("= Creating Matrix Variable and Vector Holder =\n");

        printf("= Success ! =\n");
        // random bir sekilde value lari veriyor matrix icin.
        for (int r = 0; r < M * M; r++)
        {
            A[r] = rand() % 100;
        }
        printf("\n");
        printf("======================================\n");
        printf("~ Generating random Matrix  %d X  %d   ~\n", p - 1, p - 1);
        printf("======================================\n");
        counter = 0;
        for (int r = 0; r < M * M; r++)
        {
           
            printf(" %d ", A[r]);
            counter++;
            if (counter == p - 1) {
                printf("\n");
                counter = 0;
            }
            
        }
        printf("= Success ! =\n");
        printf("\n");
        printf("Generating Vector\n");
        for (int i3 = 0; i3 < p - 1; i3++) {
            vector_x[i3] = A[i3];
            printf("~ Vector [%d] = %d ! ~\n", i3, vector_x[i3]);
        }
    }
    int* rechieve = malloc(sizeof(int) * M);

    MPI_Bcast(vector_x, p - 1, MPI_INT, 0, MPI_COMM_WORLD);
    //MPI_Bcast(A, M * M, MPI_INT, 0, MPI_COMM_WORLD);

    /*printf(" debug - - - - ");
    for (int r = 0; r < M * M; r++)
    {

        printf(" %d ", A[r]);
        counter++;
        if (counter == p - 1) {
            printf("\n");
            counter = 0;
        }

    }*/


    MPI_Scatter(A  , M  , MPI_INT, rechieve, M, MPI_INT, 0, MPI_COMM_WORLD);
    int* calculated = malloc(M * sizeof(int));
    if (rank != 0 && rank != p-1) {
        printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        for (int i = 0; i < p - 1; i++) {
            printf("= I am rank %d and i rechieved vector x as %d ! =\n", rank, vector_x[i]);
        }
        printf("= I am rank %d and i am rechieving matrix ! =\n", rank);
        for (int r = 0; r < M ; r++)
        {

            printf(" < %d > ", rechieve[r]);
        }
        printf("\n");
        printf("= I am rank %d and i am multiplaying matrix with vector x ! =\n", rank);
        for (int r = 0; r < M; r++)
        {
            calculated[r] = vector_x[r] * rechieve[r];
        }
        for (int r = 0; r < M; r++)
        {
            printf(" < %d > ", calculated[r]);
        }
        //for (int r = 0; r < M; r++)
        //{
        //    for (int c = 0; c < N; c++) {
        //        //printf("DEBUG\n");
        //        printf(" ---------------------   -%d-  --------------------- ", A[r][c]);
        //    }
        //    printf("\n");
        //}
        /*for (int i = 0; i < sizeof(p) - 1; i++) {

            printf("= I am %d and i rechieved MATRIX as %d ! =\n", rank, A[1][1]);
        }*/
        printf("\n");
        printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

    }
    int* sub = NULL;
    if (rank == 0) {
        sub = malloc(sizeof(int) * M * M);
    }

    MPI_Gather(&calculated, M, MPI_INT, sub, M * M, MPI_INT, 0, MPI_COMM_WORLD);



    /* if (rank > 0) {
         printf("= debug debug DEBUG DEBUG DEBUG ! =\n");
         int* calculatedPointVector = malloc(p * sizeof(int));

         MPI_Recv(A, size, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);
         MPI_Recv(vector_x, size, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);

         printf("Vector x size is = %d ", sizeof(vector_x));
         for (int i = 0; i < sizeof(vector_x) - 1; i++) {
             printf("= I am %d and i rechieved vector x as %d ! =\n", rank, vector_x[i]);
             printf("= I am %d and i rechieved vector A as %d ! =\n", rank, A[rank][i]);
         }*/

         //calculatedPointVector = A;

         /*for (int i = 0; i < p; i++) {
             calculatedPointVector[i] = calculatedPointVector[i] * vector_x_rec[i];
         }

         for (int i = 0; i < sizeof(calculatedPointVector) - 1; i++) {
             printf("= Calculating PointVector Multiplication %d  = %d ! =\n", i, calculatedPointVector[i]);
         }*/

         /*MPI_Send(calculatedPointVector, size, MPI_INT, 0, 0, MPI_COMM_WORLD);*/


     //printf("1\n");
     //for (int row = 1; row < p; row++) { //ne kadar process varsa (0 inci processi exclude etmiyoruz)
     //    printf("2\n");
     //    for (int collum = 0; collum < p; collum++) {
     //        printf("3\n");
     //        if (rank == row) {
     //                printf("= DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG ! =\n");
     //                A[row][collum] = A[row][collum] * (int)&vector_x[collum];
     //        }
     //    }
     //}


    printf("\n");
    if (rank == 0) {//PRINTS THE LAST MATRIX
        
        printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        printf("======================================\n");
       printf("~           Completed Matrix         ~\n");
        printf("======================================\n");
        printf("\n");

        
        printf("~~~~~~>  A %d x %d MATRIX MULTIPLIED WITH VECTOR WITH %d LONG  <~~~~~~\n", p-1, p-2, p-1);
        printf("~~~~~~>  (FIRST ROW IS USED FOR MULTIPLICATION VECTOR)  <~~~~~~\n");

        printf("\n");
        printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        printf("~~~~~~~~~~~~~          NEW MATRIX          ~~~~~~~~~~~~~ \n");
        for (int r = 0; r < M * M; r++)
        {
          
            printf("%d ", sub[r]);


            printf("\n");
        }
        printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        printf("= Success ! =\n");

    }

    printf("= Success ! =\n");
    MPI_Finalize();
}